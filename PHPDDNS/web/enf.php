<?php

include_once dirname(__FILE__).'/../setting.php';
include_once dirname(__FILE__).'/../util/getIP.php';
include_once dirname(__FILE__).'/../util/postIP.php';
include_once dirname(__FILE__).'/../util/Log.php';

if(isset($_GET['key'])){
	$key = $_GET['key'];

	if($setting_str = file_get_contents(dirname(__FILE__)."/../setting")){

		$setting = json_decode($setting_str,true);

		$data = $setting['setting'][$key];
		$nowIP = checkIP();

		log_output("enforce update   name:${data['name']} id:${data['id']} ${data['last_ip']} => ${nowIP}");

		if($data = postIP($data,$nowIP)){
			$setting['setting'][$key] = $data;
			$setting_str = json_encode($setting);
			$pointer = fopen(dirname(__FILE__)."/../setting",'w');
			fwrite($pointer,$setting_str);
			fclose($pointer);
		}
	}
}

header("Location: list.php");

