<?php
if(isset($_GET['mode'])){
	if($_GET['mode']==="add"){
		add();
	}else if($_GET['mode']==="remove"){
		remove();
	}else if($_GET['mode']==="edit"){
		edit();
	}
}

function add(){
	if( isset($_POST['name']) &&
			isset($_POST['id']) &&
			isset($_POST['pass']) &&
			isset($_POST['reg_time'])){

		$data = array(
				'name'		=> $_POST['name'],
				'id'		=> $_POST['id'],
				'pass'		=> $_POST['pass'],
				'ip'		=> isset($_POST['ip'])&&$_POST['ip']==1,
				'reg'		=> isset($_POST['reg'])&&$_POST['reg']==1,
				'reg_time'	=> $_POST['reg_time'],
				'last_time'	=> '0',
				'last_ip'	=> '0.0.0.0');

		$setting = array('setting'=>array());
		if($setting_str = file_get_contents(dirname(__FILE__)."/../setting")){
			$setting = json_decode($setting_str,true);
		}
		$setting['setting'][] = $data;
		$setting_str = json_encode($setting);
		$pointer = fopen(dirname(__FILE__)."/../setting",'w');
		fwrite($pointer,$setting_str);
		fclose($pointer);

	}
}
function remove(){
	if( isset($_GET['key'])){
		if($setting_str = file_get_contents(dirname(__FILE__)."/../setting")){
			$setting = json_decode($setting_str,true);
			unset($setting['setting'][$_GET['key']]);
			$setting_str = json_encode($setting);
			$pointer = fopen(dirname(__FILE__)."/../setting",'w');
			fwrite($pointer,$setting_str);
			fclose($pointer);
		}

	}
}
function edit(){
	if( isset($_GET['key'])){
		if($setting_str = file_get_contents(dirname(__FILE__)."/../setting")){
			$setting = json_decode($setting_str,true);
			$setting['setting'][$_GET['key']]=array(
					'name'		=> $_POST['name'],
					'id'		=> $_POST['id'],
					'pass'		=> $_POST['pass'],
					'ip'		=> isset($_POST['ip'])&&$_POST['ip']==1,
					'reg'		=> isset($_POST['reg'])&&$_POST['reg']==1,
					'reg_time'	=> $_POST['reg_time'],
					'last_time'	=> $setting['setting'][$_GET['key']]['last_time'],
					'last_ip'	=> $setting['setting'][$_GET['key']]['last_ip']);
			$setting_str = json_encode($setting);
			$pointer = fopen(dirname(__FILE__)."/../setting",'w');
			fwrite($pointer,$setting_str);
			fclose($pointer);
		}

	}
}
?>

<!DOCTYPE html>

<html lang="ja">
<head>
<meta charset="utf-8">
<link href="css/bootstrap.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<title>MyDNS IP 通知</title>
<style>
body{
	padding:10px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){


	$('.edit').click(function(){
		var modal = $('#add_modal').clone();
		rep = $(this).attr('href').replace("#edit","");
		var num = Number(rep);
		modal.find('h3').text("編集");
		modal.find("form").attr("action","list.php?mode=edit&key="+rep);
		modal.find("input[name='name']").val(data.setting[num].name);
		modal.find("input[name='id']").val(data.setting[num].id);
		modal.find("input[name='pass']").val(data.setting[num].pass);
		modal.find("input[name='ip']").attr('checked',data.setting[num].ip);
		modal.find("input[name='reg']").attr('checked',data.setting[num].reg);
		modal.find("select[name='reg_time']").val(data.setting[num].reg_time);
		modal.find("input[type='submit']").val("編集");

		modal.modal({show:true});
	});

});
</script>

</head>
<body>
<h2>MyDNS IP 通知</h2>
<div class="navbar navbar-inverse">
	<div class="navbar-inner">
		<div class="container">
			<ul class="nav">
				<li><a href="index.php">status</a></li>
				<li><a href="list.php">list</a></li>
			</ul>
		</div>
	</div>
</div>
<div id="add_modal" class="modal hide fade">
	<div class="modal-header">
	<h3>新規設定追加</h3>
	</div>
	<form action="list.php?mode=add" method="post">
	<div class="modal-body">
		<table>
			<tr>
				<td>setting name
				<td><input name="name" type="text" required="required">
			<tr>
				<td>id
				<td><input name="id" type="text" required="required">
			<tr>
				<td>pass
				<td><input name="pass" type="text" required="required">
			<tr>
				<td>通知タイミング
				<td>
				<label class="checkbox">
					<input name="ip" type="checkbox" value="1">ip変化時
				</label>
				<label class="checkbox">
					<input name="reg" type="checkbox" value="1">定期
				</label>
			<tr>
				<td>定期タイミング
				<td>
					<select name="reg_time">
						<option value="604800" >7日</option>
						<option value="518400" >6日</option>
						<option value="432000" >5日</option>
						<option value="345600" >4日</option>
						<option value="259200" >3日</option>
						<option value="172800" >2日</option>
						<option value="86400" >1日</option>
						<option value="43200" >12時間</option>
						<option value="21600" >6時間</option>
						<option value="10800" >3時間</option>
						<option value="3600" >1時間</option>
					</select>
		</table>
	</div>
	<div class="modal-footer">
		<input class="btn btn-primary" type="submit" value="追加">
		<button class="btn" data-dismiss="modal" aria-hidden="true">calcell</button>
	</div>
	</form>
</div>

<?php
if($setting_str = file_get_contents(dirname(__FILE__)."/../setting")){
	print "<script>var data=$setting_str;</script>\n";
	$setting = json_decode($setting_str,true);
	print "<table class=\"table\">\n";
	print "<tr><td><td>name<td>ID<td>PASS<td>IP<td>定期<td>定期タイミング<td>Last Time<td>Last IP<td>\n";
	foreach($setting['setting'] as $key => $value){
		print "<tr>\n";
		print "<td><a class=\"btn btn-primary\" href=\"enf.php?key=${key}\">強制通知</a>\n";
		print "<td>${value['name']}\n";
		print "<td>${value['id']}\n";
		print "<td>${value['pass']}\n";

		print "<td>". ($value['ip']?"true":"false")  ."\n";
		print "<td>". ($value['reg']?"true":"false") ."\n";


		$hour = $value['reg_time']/3600;
		if($hour>=24){
			$day=$hour/24;
			print "<td>".$day."日\n";
		}else{
			print "<td>".$hour."時間\n";
		}

		if($value['last_time'] == 0){
			print "<td>通知履歴なし\n";
		}else{
			print "<td>".date('Y-m-d H:i:s',$value['last_time'])."\n";
		}

		print "<td>".$value['last_ip']."\n";

		print "<td><a class=\"btn btn-primary edit\" href=\"#edit$key\">編集</a> <a class=\"btn btn-danger\" href=\"list.php?mode=remove&key=$key\">削除</a>\n";
	}
	print "</table>\n";
}

?>

<a class="btn btn-primary" href="#add_modal" data-toggle="modal">追加</a>

</body>
</html>