<?php
include_once dirname(__FILE__).'/../setting.php';
include_once dirname(__FILE__).'/Log.php';

function postIP($data,$newIP){
	global $urlBase;
	$id		= $data['id'];
	$pass	= $data['pass'];

	$url = str_replace('[id]',$id,$urlBase);
	$url = str_replace('[pass]',$pass,$url);
	if($html = getHTML($url)){
		preg_match_all("/<DT>([^<\s]*|[^\s]*\s[^<]*)\s?:<\/DT><DD>([^<]*)<\/DD>/m",$html,$arr,PREG_SET_ORDER);
		$ress = array();
		foreach($arr as $value){
			$ress[$value[1]] = $value[2];
		}
		log_output("Success name:${data['name']} id:${ress['MASTERID']} REMOTE ADDRESS:${ress['REMOTE ADDRESS']} SERVER ADDRESS:${ress['SERVER ADDRESS']}");

		$data['last_time']	= time();
		$data['last_ip']	= $newIP;

		return $data;

	}else{
		log_output("Error   name:${data['name']} id:${data['id']} newIP:${newIP}");
		return false;
	}

}

function getHTML($url){
	$session = curl_init();
	curl_setopt ($session , CURLOPT_URL, $url);
	curl_setopt($session, CURLOPT_HEADER, false);
	curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($session);
	curl_close($session);
	return $response;
}