<?php
include_once dirname(__FILE__).'/../setting.php';

function checkIP(){
	global $ip_check_address;
	$handle =  fsockopen($ip_check_address);
	$ip = fgets($handle);
	fclose($handle);
	preg_match("/\d+\.\d+\.\d+\.\d+/",$ip,$match);
	return $match[0];
}