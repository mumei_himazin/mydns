<?php
include_once dirname(__FILE__).'/setting.php';
include_once dirname(__FILE__).'/util/getIP.php';
include_once dirname(__FILE__).'/util/postIP.php';
include_once dirname(__FILE__).'/util/Log.php';

$nowIP = checkIP();

if($setting_str = file_get_contents(dirname(__FILE__)."/setting")){
	$setting = json_decode($setting_str,true);
	$datas = $setting['setting'];
	foreach($datas as $key => $data){
		$flag = false;

		if( $data['ip'] && $data['last_ip'] !== $nowIP ){
			log_output("IP Chenge   name:${data['name']} id:${data['id']} ${data['last_ip']} => ${nowIP}");
			$flag = true;
		}
		if( $data['reg'] && time() >= $data['last_time']+$data['reg_time'] ){
			$date = date("Y-m-d H:i:s",$data['last_time']);
			log_output("time exec   name:${data['name']} id:${data['id']} last:${date}");
			$flag = true;
		}

		if($flag && $newData = postIP($data,$nowIP))$datas[$key]=$newData;
	}
	$setting['setting'] = $datas;
	$setting_str = json_encode($setting);
	$pointer = fopen(dirname(__FILE__)."/setting",'w');
	fwrite($pointer,$setting_str);
	fclose($pointer);

}